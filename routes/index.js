var express = require('express');
var router = express.Router();

var passport = require('passport');

var User = require('../models/user');
var Product = require('../models/product');

const request = require('request');
const uuidv1 = require('uuid/v1');

/* GET home page. */
router.get('/', function(req, res, next) {
  Product.find().sort({ date_added: -1 }).limit(4).exec(function(err, products) {
    if (err) return next(err);
    res.render('index', { listings: products });
  });
});

/* GET signup page. */
router.get('/signup', function(req, res, next) {
  if (req.user) {
    res.redirect('/dashboard');
  } else {
    res.render('signup');
  }
});

router.post('/login', passport.authenticate('local', {
  successRedirect: '/dashboard',
  failureRedirect: '/signup'
}));

router.get('/logout', function(req, res, next) {
  req.logout();
  res.redirect('/');
})

router.post('/signup', function(req, res, next) {
  // Connect to Paypal for setup links
  getPayPalLinks(uuidv1(), (paypal) => {
    User.create({
      business_name: req.body.business_name,
      business_address: req.body.business_address,
      city: req.body.city,
      state: req.body.state,
      zip_code: req.body.zip_code,
      email: req.body.email,
      password: req.body.password,
      paypal_links: paypal.body,
      uuid: paypal.uuid
    }, function(err, user) {
      if (err) return next(err);
      req.login(user, function(err) {
        if (err) return next(err);
        return res.redirect('/dashboard');
      });
    });
  });
});

router.post('/order', function(req, res, next) {
  makePayPalOrder(req.body.paypal_merch_id, (order) => {
    return order;
  });
});
router.post('/capture', function(req, res, next) {
  capturePayPalOrder(req.body.order_id);
});

router.get('/dashboard', function(req, res, next) {
  if (req.user) {

    const getProducts = () => {
      Product.find({ seller: req.user._id }, function(err, products) {
        if (err) return next(err);
        res.render('dashboard', {
          user: req.user,
          listings: products,
          paypal_connected: req.user.paypal_merch_id
        });
      });
    };

    // Update merchantId, if redirected from PayPal
    const { merchantIdInPayPal } = req.query;
    if (merchantIdInPayPal && !req.user.paypal_merch_id) {
      User.findOne({ _id: req.user._id }, function(err, user) {
        if (err) return next(err);
        user.paypal_merch_id = merchantIdInPayPal;
        user.save();

        // Products
        req.user = user;
        getProducts();
      });
    } else {

      // Products
      getProducts();
    }
  } else {
    res.render('signup');
  }
});

/*
  PayPal Integration
 */
const client_id = 'AeUaTIQynHfU5M-wqiFdjhLys1Vll5aX5d5pXFvTM2p6VePAkwSeDYNF6c0NdgpkQ3XQjEKxbIigIxF4';
const secret = 'EBAMumyI2v1rWp6ZTFImvFferZ3CbVeT9EoKSs2kxBUScxegPhsTtxHCf66kNb0Ph-yXOaR8NmE2ZNsa';

const getPayPalAccessToken = (next) => {
  const auth = Buffer.from(client_id + ':' + secret).toString('base64');
  request({
    url: 'https://api.sandbox.paypal.com/v1/oauth2/token',
    method: "POST",
    body: 'grant_type=client_credentials',
    headers: {
        'Accept': 'application/json',
        'Accept-Language': 'en_US',
        'Authorization': 'Basic ' + auth,
    }
  }, (err, res, body) => {
    if (err) { return console.error(err); }
    next(JSON.parse(body).access_token);
  });
};

// Seller Setup
const payPalLinksRequest = (uuid) => ({
  "customer_data": {
    "partner_specific_identifiers": [{
      "type": "TRACKING_ID",
      "value": uuid
    }]
  },
  "web_experience_preference": {
    "return_url": "http://localhost:3000/dashboard",
  },
});
const getPayPalLinks = (uuid, next) => {
getPayPalAccessToken((access_token) => {
  request({
    url: 'https://api.sandbox.paypal.com/v1/customer/partner-referrals',
    method: "POST",
    json: payPalLinksRequest(uuid),
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + access_token
    }
  }, (err, res, body) => {
    if (err) { return console.error(err); }
    next({ uuid, body });
  });
});
};

// Buy from Seller (Orders)
const payPalOrderRequest = (merchant_id) => ({
 "intent": "CAPTURE",
 "purchase_units": [{
   "amount": {
     "currency_code": "USD",
     "value": "100.00"
   },
   "payee": {
     "merchant_id": merchant_id
   },
   "payment_instruction": {
     "disbursement_mode": "INSTANT",
     "platform_fees": [{
       "amount": {
         "currency_code": "USD",
         "value": "25.00"
       }
     }]
   }
 }]
});
const makePayPalOrder = (merchant_id, next) => {
getPayPalAccessToken((access_token) => {
  request({
    url: 'https://api.sandbox.paypal.com/v2/checkout/orders',
    method: "POST",
    json: payPalOrderRequest(merchant_id),
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + access_token
    }
  }, (err, res, body) => {
    if (err) { return console.error(err); }
    next(body);
  });
});
}

// Capture Funds (move funds to Seller)
const capturePayPalOrder = (order_id, next) => {
getPayPalAccessToken((access_token) => {
  request({
    url: 'https://api.paypal.com/v2/checkout/orders/' + order_id + '/capture',
    method: "POST",
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + access_token
    }
  }, (err, res, body) => {
    if (err) { return console.error(err); }
    next(body);
  });
});
}

module.exports = router;
