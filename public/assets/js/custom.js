jQuery( document ).ready(function( $ ) {


	"use strict";
		$('#order-now').click(function(event) {
			paypal.Buttons({
                createOrder: function(data, actions) {
                  return fetch('/order', {
                    method: 'POST',
                    body: JSON.stringify({
                        paypal_merch_id: $('#paypal_merch_id').val()
                    })
                  }).then(function(res) {
                    return res.json();
                  }).then(function(data) {
                    return data.id;
                  });
                },
                onApprove: function(data, actions) {
                  return fetch('/capture', {
                    method: 'POST',
                    body: JSON.stringify({
                        order_id: data.orderID
                    })
                  }).then(function(res){
                    if (!res.ok) {
                      alert('Something went wrong');
                    }
                  });
                }
            }).render('#paypal-checkout');
			$('#paypal-checkout').data('active', true);
			$('#order-now').hide();
			return false;
		});

		$('.owl-carousel').owlCarousel({
		    items:4,
		    lazyLoad:true,
		    loop:true,
		    dots:true,
		    margin:20,
		    responsiveClass:true,
			    responsive:{
			        0:{
			            items:1,
			        },
			        600:{
			            items:2,
			        },
			        1000:{
			            items:4,
			        }
			    }
		});

		/* activate jquery isotope */
		  var $container = $('.posts').isotope({
		    itemSelector : '.item',
		    isFitWidth: true
		  });

		  $(window).smartresize(function(){
		    $container.isotope({
		      columnWidth: '.col-sm-3'
		    });
		  });

		  $container.isotope({ filter: '*' });

		    // filter items on button click
		  $('#filters').on( 'click', 'button', function() {
		    var filterValue = $(this).attr('data-filter');
		    $container.isotope({ filter: filterValue });
		});


		$('#carousel').flexslider({
		    animation: "slide",
		    controlNav: false,
		    animationLoop: false,
		    slideshow: false,
		    itemWidth: 210,
		    itemMargin: 5,
		    asNavFor: '#slider'
		});

		$('#slider').flexslider({
		    animation: "slide",
		    controlNav: false,
		    animationLoop: false,
		    slideshow: false,
		    sync: "#carousel"
		});

		$('#slider-secondary').flexslider({
		    animation: "slide",
		    controlNav: false,
		    animationLoop: false,
		    slideshow: false,
		    sync: "#carousel"
		});

});
